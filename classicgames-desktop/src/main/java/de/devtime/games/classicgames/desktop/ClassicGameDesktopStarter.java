package de.devtime.games.classicgames.desktop;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics.DisplayMode;
import com.badlogic.gdx.Graphics.Monitor;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Preferences;
import com.badlogic.gdx.graphics.Color;

import de.devtime.games.classicgames.core.GameSettings;
import de.devtime.games.classicgames.core.screen.View;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ClassicGameDesktopStarter {

  private enum StartScreen {
    DEFAULT, SETTINGS
  }

  public static void main(String[] args) {
    Options options = new Options();
    Option optScreen = new Option("screen", true,
        "Start the application in the given screen. Available screens are: default, settings");
    options.addOption(optScreen);

    CommandLineParser parser = new DefaultParser();
    try {
      CommandLine cmd = parser.parse(options, args);
      if (cmd.hasOption(optScreen)) {
        String screenValue = cmd.getOptionValue(optScreen);
        log.info("screenValue: {}", screenValue);
        switch (screenValue) {
          case "settings" -> new ClassicGameDesktopStarter(StartScreen.SETTINGS);
          default -> new ClassicGameDesktopStarter(StartScreen.DEFAULT);
        }
      } else {
        new ClassicGameDesktopStarter(StartScreen.DEFAULT);
      }
    } catch (ParseException e) {
      log.error(e.getMessage(), e);
    }
  }

  private Lwjgl3Application app;
  private Game game;
  private StartScreen startScreen;

  public ClassicGameDesktopStarter(StartScreen startScreen) {
    super();

    this.startScreen = startScreen;
    Lwjgl3Preferences settings = new Lwjgl3Preferences("classicgames", ".prefs");

    boolean fullscreen = settings.getBoolean(GameSettings.PREF_FULLSCREEN, false);
    boolean maximized = settings.getBoolean(GameSettings.PREF_MAXIMIZED, false);
    Monitor primaryMonitor = getMonitor(settings.getString(GameSettings.PREF_MONITOR));
    DisplayMode primaryDisplayMode = getDisplayMode(settings.getString(GameSettings.PREF_MONITOR),
        settings.getString(GameSettings.PREF_DISPLAY_MODE));
    boolean vsync = settings.getBoolean(GameSettings.PREF_VSYNC);

    initialGraphicsConfiguration(fullscreen, maximized, primaryMonitor, primaryDisplayMode, vsync);
  }

  public static Monitor getMonitor(String monitorName) {
    Monitor monitorToUse = Lwjgl3ApplicationConfiguration.getPrimaryMonitor();
    Monitor[] monitors = Lwjgl3ApplicationConfiguration.getMonitors();
    for (Monitor monitor : monitors) {
      if (Objects.equals(monitor.name, monitorName)) {
        monitorToUse = monitor;
        break;
      }
    }
    log.info("Monitor to use: {}", monitorToUse.name);
    return monitorToUse;
  }

  public static DisplayMode getDisplayMode(String monitorName, String displayModeValue) {
    Monitor monitor = getMonitor(monitorName);
    DisplayMode displayModeToUse = Lwjgl3ApplicationConfiguration.getDisplayMode(monitor);
    DisplayMode[] displayModes = Lwjgl3ApplicationConfiguration.getDisplayModes(monitor);
    for (DisplayMode displayMode : displayModes) {
      if (Objects.equals(GameSettings.buildDisplayModeConfigValue(displayMode), displayModeValue)) {
        displayModeToUse = displayMode;
        break;
      }
    }
    return displayModeToUse;
  }

  private void initialGraphicsConfiguration(boolean fullscreen, boolean maximized, Monitor monitor,
      DisplayMode gameDisplayMode, boolean vsync) {
    log.info("fullscreen: {}, maximized: {}, monitor: {}-{}x{}, gameDisplayMode: {}x{}, vsync: {}",
        fullscreen, maximized, monitor.name, monitor.virtualX, monitor.virtualY, gameDisplayMode.width,
        gameDisplayMode.height, vsync);

    Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
    config.setTitle("Mastermind");
    config.setInitialBackgroundColor(Color.BLACK);

    config.useVsync(vsync);
    if (fullscreen) {
      config.setFullscreenMode(gameDisplayMode);
    } else if (maximized) {
      config.setWindowedMode(gameDisplayMode.width, gameDisplayMode.height);
      config.setWindowPosition(-1, -1);
      config.setDecorated(false);
      config.setResizable(false);
    } else {
      config.setWindowedMode(gameDisplayMode.width, gameDisplayMode.height);
      DisplayMode currentDeviceDisplayMode = Lwjgl3ApplicationConfiguration.getDisplayMode(monitor);
      log.info("currentDeviceDisplayMode: {}x{}", currentDeviceDisplayMode.width, currentDeviceDisplayMode.height);
      config.setWindowPosition(monitor.virtualX + currentDeviceDisplayMode.width / 2 - gameDisplayMode.width / 2,
          monitor.virtualY + currentDeviceDisplayMode.height / 2 - gameDisplayMode.height / 2);
      config.setDecorated(true);
      config.setResizable(false);
    }

    log.info("create window");
    if (this.startScreen == StartScreen.DEFAULT) {
      this.game = new ClassicGamesStarter(this, fullscreen || maximized, View.MAIN_MENU);
    }
    if (this.startScreen == StartScreen.SETTINGS) {
      this.game = new ClassicGamesStarter(this, fullscreen || maximized, View.SETTINGS);
    }
    this.app = new Lwjgl3Application(this.game, config);
  }

  public void restartWithNewGraphicSettings() {
    List<String> args = new ArrayList<>();
    args.add("-screen");
    args.add("settings");
    new Thread(() -> {
      try {
        JavaProcess.exec(ClassicGameDesktopStarter.class, args);
      } catch (IOException e) {
        log.error(e.getMessage(), e);
      } catch (InterruptedException e) {
        log.error(e.getMessage(), e);
        Thread.currentThread().interrupt();
      }
    }).start();
    Gdx.app.exit();
  }
}
