package de.devtime.games.classicgames.desktop;

import java.awt.Dimension;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import de.devtime.games.classicgames.core.AssetConstants;
import de.devtime.games.classicgames.core.GameController;
import de.devtime.games.classicgames.core.GameSettings;
import de.devtime.games.classicgames.core.model.GameItem.ClassicGame;
import de.devtime.games.classicgames.core.screen.MainMenuScreen;
import de.devtime.games.classicgames.core.screen.SettingsScreen;
import de.devtime.games.classicgames.core.screen.View;
import de.devtime.games.mastermind.screen.MastermindMainMenuScreen;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ClassicGamesStarter extends Game implements GameController {

  public static AssetManager getAssetManager() {
    return ASSETS;
  }

  public static TextureAtlas getTextureAtlas() {
    if (atlas == null) {
      throw new NullPointerException("TextureAtlas is not initialized! Please initialize AssetManager first.");
    }
    return atlas;
  }

  public static void inizializeTextureAtlas() {
    atlas = ASSETS.get(AssetConstants.IMAGES_ATLAS, TextureAtlas.class);
  }

  private static final AssetManager ASSETS = new AssetManager();
  private static TextureAtlas atlas = null;

  private ClassicGameDesktopStarter starter;
  private Screen mainMenuScreen;
  private Screen settingsScreen;
  private boolean maximized;
  private GameSettings settings;
  private View startScreen;

  public ClassicGamesStarter(ClassicGameDesktopStarter starter, boolean maximized, View startScreen) {
    super();

    this.starter = starter;
    this.maximized = maximized;
    this.startScreen = startScreen;
    this.settings = new GameSettings();
  }

  @Override
  public void create() {
    log.debug("create game ...");

    ASSETS.load(AssetConstants.IMAGES_ATLAS, TextureAtlas.class);
    ASSETS.load(AssetConstants.SGX_SKIN_JSON, Skin.class);
    ASSETS.finishLoading();
    inizializeTextureAtlas();

    changeScreen(this.startScreen);
  }

  @Override
  public AssetManager getAssets() {
    return ClassicGamesStarter.ASSETS;
  }

  @Override
  public void exit() {
    Gdx.app.exit();
  }

  @Override
  public Dimension getViewDimension() {
    Dimension dimension = new Dimension();
    dimension.width = Gdx.graphics.getDisplayMode().width;
    dimension.height = Gdx.graphics.getDisplayMode().height;
    if (!isMaximized()) {
      dimension.width = Gdx.graphics.getWidth();
      dimension.height = Gdx.graphics.getHeight();
    }
    return dimension;
  }

  @Override
  public void changeScreen(View screen) {
    log.debug("screen: {}", screen);
    switch (screen) {
      case MAIN_MENU:
        if (this.mainMenuScreen == null) {
          this.mainMenuScreen = new MainMenuScreen(this);
        }
        setScreen(this.mainMenuScreen);
      break;
      case SETTINGS:
        if (this.settingsScreen == null) {
          this.settingsScreen = new SettingsScreen(this);
        }
        setScreen(this.settingsScreen);
      break;
      default:
        throw new IllegalArgumentException("Unexpected value: " + screen);
    }
  }

  @Override
  public boolean isMaximized() {
    return this.maximized;
  }

  @Override
  public GameSettings getSettings() {
    return this.settings;
  }

  @Override
  public void restartWithNewGraphicSettings() {
    this.starter.restartWithNewGraphicSettings();
  }

  @Override
  public void startGame(ClassicGame game) {
    log.info("game: {}", game);
    switch (game) {
      case MASTERMIND:
        setScreen(new MastermindMainMenuScreen(this));
      break;
      default:
        throw new IllegalArgumentException("Unknown game: " + game);
    }
  }

  @Override
  public TextureAtlas getAtlas() {
    return atlas;
  }
}
