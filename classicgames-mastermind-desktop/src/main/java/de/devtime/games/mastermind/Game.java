package de.devtime.games.mastermind;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Arrays;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.LineBorder;

import org.apache.commons.lang3.RandomUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Game extends JFrame {

  private static final Dimension WINDOW_SIZE = new Dimension(1000, 750);

  private static final int ROWS = 10;
  private static final int CODE_LENGTH = 4;

  private static final int CODE_FIELD_SIZE = 50;
  private static final ImageIcon EMPTY_SLOT_SMALL = new ImageIcon(
      Game.class.getResource("/images/emptyslot_small.png"));
  private static final ImageIcon EMPTY_SLOT_LARGE = new ImageIcon(
      Game.class.getResource("/images/emptyslot_large.png"));

  private static final int CORRECT_COLOR_AT_CORRECT_POSITION = -2;
  private static final int CORRECT_COLOR_AT_WRONG_POSITION = -1;
  private static final int NO_VALUE = -1;

  private static final ImageIcon BLACK_SMALL = new ImageIcon(Game.class.getResource("/images/black_small.png"));
  private static final ImageIcon WHITE_SMALL = new ImageIcon(Game.class.getResource("/images/white_small.png"));

  private static final ImageIcon BLUE = new ImageIcon(Game.class.getResource("/images/blue.png"));
  private static final ImageIcon BROWN = new ImageIcon(Game.class.getResource("/images/brown.png"));
  //  private static final ImageIcon CYAN = new ImageIcon(Game.class.getResource("/images/cyan.png"));
  private static final ImageIcon GREEN = new ImageIcon(Game.class.getResource("/images/green.png"));
  private static final ImageIcon ORANGE = new ImageIcon(Game.class.getResource("/images/orange.png"));
  private static final ImageIcon PINK = new ImageIcon(Game.class.getResource("/images/pink.png"));
  private static final ImageIcon RED = new ImageIcon(Game.class.getResource("/images/red.png"));
  private static final ImageIcon VIOLETTE = new ImageIcon(Game.class.getResource("/images/violette.png"));
  private static final ImageIcon YELLOW = new ImageIcon(Game.class.getResource("/images/yellow.png"));

  private static final ImageIcon[] COLOR_ICONS = new ImageIcon[8];

  public static void main(String[] args) {
    COLOR_ICONS[0] = BLUE;
    COLOR_ICONS[1] = BROWN;
    COLOR_ICONS[2] = GREEN;
    COLOR_ICONS[3] = ORANGE;
    COLOR_ICONS[4] = PINK;
    COLOR_ICONS[5] = RED;
    COLOR_ICONS[6] = VIOLETTE;
    COLOR_ICONS[7] = YELLOW;

    new Game();
  }

  private JPanel codePanel = new JPanel();
  private JLabel[] code = new JLabel[CODE_LENGTH];
  private JLabel[][] codes = new JLabel[ROWS][CODE_LENGTH];
  private JLabel[][] lblMarker = new JLabel[ROWS][CODE_LENGTH];
  private JLabel[] colors = new JLabel[COLOR_ICONS.length];

  private int[] codeToFind = new int[CODE_LENGTH];
  private int[] currentCode = new int[CODE_LENGTH];
  private int activeColor = NO_VALUE;
  private int currentRow = 0;

  private Game() {
    super();

    javax.swing.SwingUtilities.invokeLater(this::createGUI);
  }

  private void createGUI() {
    setSize(WINDOW_SIZE);
    setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        exit();
      }
    });
    createBoard();
    reset();
    setVisible(true);
  }

  private void createBoard() {
    setLayout(new GridBagLayout());
    GridBagConstraints gc = new GridBagConstraints();

    int y = 0;
    this.codePanel.setLayout(new GridLayout(1, CODE_LENGTH, 0, 0));
    for (int i = 0; i < CODE_LENGTH; i++) {
      this.code[i] = new JLabel();
      this.code[i].setPreferredSize(new Dimension(CODE_FIELD_SIZE, CODE_FIELD_SIZE));
      this.code[i].setBorder(new LineBorder(Color.BLACK));
      this.code[i].setIcon(EMPTY_SLOT_LARGE);
      this.code[i].setHorizontalAlignment(SwingConstants.CENTER);
      this.codePanel.add(this.code[i]);
    }
    GCUtil.configureGC(gc, 0, y, GCUtil.EAST, GCUtil.NONE, 0.0, 0.0, 1, 1, GUIConstants.NO_INSETS);
    add(this.codePanel, gc);

    for (int i = 0; i < ROWS; i++) {
      JPanel row = createRow(i);
      y++;
      GCUtil.configureGC(gc, 0, y, GCUtil.WEST, GCUtil.NONE, 0.0, 0.0, 1, 1, GUIConstants.NO_INSETS);
      add(row, gc);
    }

    GCUtil.configureGC(gc, 1, y, GCUtil.WEST, GCUtil.NONE, 0.0, 0.0, 1, 1, GUIConstants.LEFT_INSETS);
    add(createColorPanel(), gc);
  }

  private JPanel createColorPanel() {
    JPanel colorPanel = new JPanel();
    colorPanel.setLayout(new GridLayout(1, 8, 0, 0));

    for (int i = 0; i < COLOR_ICONS.length; i++) {
      final int index = i;
      this.colors[i] = new JLabel();
      this.colors[i].setPreferredSize(new Dimension(CODE_FIELD_SIZE, CODE_FIELD_SIZE));
      this.colors[i].setBorder(new LineBorder(Color.BLACK));
      this.colors[i].setIcon(COLOR_ICONS[i]);
      this.colors[i].setHorizontalAlignment(SwingConstants.CENTER);
      this.colors[i].addMouseListener(new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
          Game.this.activeColor = index;
          Cursor cursor = Toolkit.getDefaultToolkit().createCustomCursor(COLOR_ICONS[index].getImage(),
              new Point(CODE_FIELD_SIZE / 2, CODE_FIELD_SIZE / 2),
              "colorPicked");
          setCursor(cursor);
        }
      });
      colorPanel.add(this.colors[i]);
    }
    return colorPanel;
  }

  private JPanel createRow(int rowIndex) {
    JPanel row = new JPanel();
    row.setLayout(new GridBagLayout());

    createMarkerRow(rowIndex, row);
    createCodeRow(rowIndex, row);

    return row;
  }

  private void createCodeRow(final int rowIndex, JPanel row) {
    GridBagConstraints gc = new GridBagConstraints();
    for (int i = 0; i < CODE_LENGTH; i++) {
      final int colIndex = i;
      this.codes[rowIndex][i] = new JLabel();
      this.codes[rowIndex][i].setPreferredSize(new Dimension(CODE_FIELD_SIZE, CODE_FIELD_SIZE));
      this.codes[rowIndex][i].setBorder(new LineBorder(Color.BLACK));
      this.codes[rowIndex][i].setIcon(EMPTY_SLOT_LARGE);
      this.codes[rowIndex][i].setHorizontalAlignment(SwingConstants.CENTER);
      this.codes[rowIndex][i].addMouseListener(new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
          if ((e.getButton() == MouseEvent.BUTTON1) && (Game.this.activeColor > -1)) {
            if (rowIndex == Game.this.currentRow) {
              Game.this.currentCode[colIndex] = Game.this.activeColor;
            }
            Game.this.codes[rowIndex][colIndex].setIcon(COLOR_ICONS[Game.this.activeColor]);
            check();
          }
          if (e.getButton() == MouseEvent.BUTTON3) {
            if (rowIndex == Game.this.currentRow) {
              Game.this.currentCode[colIndex] = NO_VALUE;
            }
            Game.this.codes[rowIndex][colIndex].setIcon(EMPTY_SLOT_LARGE);
          }
          setCursor(Cursor.getDefaultCursor());
        }
      });
      GCUtil.configureGC(gc, i + 1, 0, GCUtil.WEST, GCUtil.NONE, 0.0, 0.0, 1, 1, GUIConstants.NO_INSETS);
      row.add(this.codes[rowIndex][i], gc);
    }
  }

  private void createMarkerRow(int rowIndex, JPanel row) {
    JPanel markerPanel = new JPanel();
    markerPanel.setLayout(new GridLayout(2, ROWS / 2, 0, 0));
    for (int i = 0; i < CODE_LENGTH; i++) {
      this.lblMarker[rowIndex][i] = new JLabel();
      this.lblMarker[rowIndex][i].setPreferredSize(new Dimension(CODE_FIELD_SIZE / 2, CODE_FIELD_SIZE / 2));
      this.lblMarker[rowIndex][i].setBorder(new LineBorder(Color.BLACK));
      this.lblMarker[rowIndex][i].setIcon(EMPTY_SLOT_SMALL);
      this.lblMarker[rowIndex][i].setHorizontalAlignment(SwingConstants.CENTER);
      markerPanel.add(this.lblMarker[rowIndex][i]);
    }
    row.add(markerPanel);
  }

  private void reset() {
    for (int i = 0; i < CODE_LENGTH; i++) {
      this.codeToFind[i] = RandomUtils.nextInt(0, COLOR_ICONS.length);
      this.currentCode[i] = NO_VALUE;
      log.info("codeToFind: {}", this.codeToFind[i]);
    }
    this.codePanel.setVisible(false);
  }

  private void showCodeToFind() {
    for (int i = 0; i < CODE_LENGTH; i++) {
      this.code[i].setIcon(COLOR_ICONS[this.codeToFind[i]]);
    }
    this.codePanel.setVisible(true);
  }

  private void check() {
    if (isCurrentRowComplete()) {
      int marker[] = new int[CODE_LENGTH];
      correctColorPosition(marker);
      containsColor(marker);
      int correctColorAmount = 0;
      Arrays.sort(marker);
      for (int i = 0; i < CODE_LENGTH; i++) {
        if (marker[i] == CORRECT_COLOR_AT_CORRECT_POSITION) {
          correctColorAmount++;
          this.lblMarker[this.currentRow][i].setIcon(BLACK_SMALL);
        }
        if (marker[i] == CORRECT_COLOR_AT_WRONG_POSITION) {
          this.lblMarker[this.currentRow][i].setIcon(WHITE_SMALL);
        }
      }
      for (int i = 0; i < CODE_LENGTH; i++) {
        this.currentCode[i] = NO_VALUE;
      }
      this.currentRow++;
      if (correctColorAmount == CODE_LENGTH || this.currentRow == ROWS) {
        showCodeToFind();
      }
    }
  }

  private void correctColorPosition(int[] marker) {
    for (int i = 0; i < CODE_LENGTH; i++) {
      if (this.codeToFind[i] == this.currentCode[i]) {
        marker[i] = CORRECT_COLOR_AT_CORRECT_POSITION;
      }
    }
  }

  private void containsColor(int[] marker) {
    boolean[] used = new boolean[CODE_LENGTH];
    for (int i = 0; i < CODE_LENGTH; i++) {
      used[i] = marker[i] == CORRECT_COLOR_AT_CORRECT_POSITION;
    }
    for (int i = 0; i < CODE_LENGTH; i++) { // iterate over currentCode
      if (marker[i] != CORRECT_COLOR_AT_CORRECT_POSITION) { // only check colors that does not already match with the code to find
        for (int j = 0; j < CODE_LENGTH; j++) { // iterate over codeToFind
          if (!used[j] && marker[i] != CORRECT_COLOR_AT_CORRECT_POSITION && this.codeToFind[j] == this.currentCode[i]) {
            marker[i] = CORRECT_COLOR_AT_WRONG_POSITION;
            used[j] = true;
            break;
          }
        }
      }
    }
  }

  private boolean isCurrentRowComplete() {
    boolean result = true;
    for (int i = 0; i < CODE_LENGTH; i++) {
      if (this.currentCode[i] == NO_VALUE) {
        result = false;
        break;
      }
    }
    return result;
  }

  public void exit() {
    System.exit(0);
  }
}
