package de.devtime.games.classicgames.core.util;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;

public class TextureAtlasCreator {

  public static void main(String[] args) {

    TexturePacker.Settings settings = new TexturePacker.Settings();
    settings.maxHeight = 2048;
    settings.maxWidth = 2048;
    settings.edgePadding = true;
    settings.duplicatePadding = true;
    settings.filterMin = Texture.TextureFilter.Linear;
    settings.filterMag = Texture.TextureFilter.Linear;
    TexturePacker.process(settings,
        "/media/truecrypt2/development/dev-time/gitlab/mastermind/assets/images",
        "/media/truecrypt2/development/dev-time/gitlab/mastermind/assets/atlas", "image-atlas");
  }

}
