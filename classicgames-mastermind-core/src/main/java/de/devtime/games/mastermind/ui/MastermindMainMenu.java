package de.devtime.games.mastermind.ui;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import de.devtime.games.classicgames.core.AssetConstants;
import de.devtime.games.classicgames.core.GameController;
import de.devtime.games.mastermind.MastermindController;
import de.devtime.games.mastermind.MastermindController.View;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MastermindMainMenu extends Table {

  public static final int MENU_WIDTH = 300;

  private static final int BORDER_THICKNESS = 3;
  private static final int BUTTON_WIDTH = 200;
  private static final int BUTTON_HEIGHT = 40;
  private static final int BUTTON_PADDING = 20;

  private final GameController game;
  private final MastermindController controller;

  private TextButton singleplayer;
  private TextButton back;

  public MastermindMainMenu(GameController game, MastermindController controller) {
    super();
    log.info("create");

    this.game = game;
    this.controller = controller;

    createUI();
    configureListener();
  }

  private void createUI() {
    AssetManager assets = this.game.getAssets();
    Skin skin = assets.get(AssetConstants.SGX_SKIN_JSON, Skin.class);

    this.singleplayer = new TextButton("Einzelspieler", skin);
    this.back = new TextButton("Zum Hauptmenu", skin);

    Table table = new Table(skin);
    table.setPosition(BORDER_THICKNESS, 0);
    table.setWidth(MENU_WIDTH - 2f * BORDER_THICKNESS);
    table.setHeight(this.game.getViewDimension().height);
    table.top().left().pad(50, (MENU_WIDTH - BUTTON_WIDTH) / 2f, 0, 0);
    table.row().width(5);
    table.add(this.singleplayer).width(BUTTON_WIDTH).height(BUTTON_HEIGHT).uniformX();
    table.row().pad(BUTTON_PADDING, 0, 0, 0);
    table.add(this.back).width(BUTTON_WIDTH).height(BUTTON_HEIGHT).uniformX();
    table.setBackground(new TextureRegionDrawable(this.game.getAtlas().findRegion(AssetConstants.MENU_BACKGROUND)));

    setWidth(MENU_WIDTH);
    setHeight(this.game.getViewDimension().height);
    add(new Image(new TextureRegionDrawable(this.game.getAtlas().findRegion(AssetConstants.BLACK_DOT))))
        .width(BORDER_THICKNESS)
        .height(this.game.getViewDimension().height);
    add(table)
        .width(MENU_WIDTH - 2f * BORDER_THICKNESS)
        .height(this.game.getViewDimension().height);
    add(new Image(new TextureRegionDrawable(this.game.getAtlas().findRegion(AssetConstants.BLACK_DOT))))
        .width(BORDER_THICKNESS)
        .height(this.game.getViewDimension().height);
  }

  private void configureListener() {
    this.singleplayer.addListener(new ChangeListener() {
      @Override
      public void changed(ChangeEvent event, Actor actor) {
        MastermindMainMenu.this.controller.changeScreen(View.SETTINGS);
      }
    });

    this.back.addListener(new ChangeListener() {
      @Override
      public void changed(ChangeEvent event, Actor actor) {
        MastermindMainMenu.this.game.changeScreen(de.devtime.games.classicgames.core.screen.View.MAIN_MENU);
      }
    });
  }

}
