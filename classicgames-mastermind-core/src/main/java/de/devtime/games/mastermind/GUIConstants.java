package de.devtime.games.mastermind;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.text.ParseException;

import javax.swing.border.Border;
import javax.swing.text.MaskFormatter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GUIConstants {

  public static final int DEFAULT_DISTANCE = 5;
  public static final int DEFAULT_BUTTON_DISTANCE = 2;
  public static final Insets NO_INSETS = new Insets(0, 0, 0, 0);
  public static final Insets TOP_INSETS = new Insets(DEFAULT_DISTANCE, 0, 0, 0);
  public static final Insets LEFT_INSETS = new Insets(0, DEFAULT_DISTANCE, 0, 0);
  public static final Insets BOTTOM_INSETS = new Insets(0, 0, DEFAULT_DISTANCE, 0);
  public static final Insets RIGHT_INSETS = new Insets(0, 0, 0, DEFAULT_DISTANCE);
  public static final Insets LEFT_TOP_INSETS = new Insets(DEFAULT_DISTANCE, DEFAULT_DISTANCE, 0, 0);
  public static final Insets LEFT_TOP_RIGHT_INSETS = new Insets(DEFAULT_DISTANCE, DEFAULT_DISTANCE, 0,
      DEFAULT_DISTANCE);
  public static final Insets ALL_INSETS = new Insets(DEFAULT_DISTANCE, DEFAULT_DISTANCE, DEFAULT_DISTANCE,
      DEFAULT_DISTANCE);
  public static final int DEFAULT_TEXTFIELD_COLUMNS = 10;
  public static final String VERSION_MASK = "#.##.###";
  public static final MaskFormatter VERSION_MASK_FORMATTER = getVersionMask();
  public static final Dimension PREFERRED_TEXTFIELD_SIZE = new Dimension(21, 21);
  public static final Color DEACTIVATED_TEXTCOMPONENT_COLOR = new Color(200, 200, 200);
  public static final Color DEACTIVATED_TEXTCOMPONENT_LIGHT_COLOR = new Color(235, 235, 235);
  public static final Border DEFAULT_BORDER = new InputFieldLineBorder(Color.GRAY, 1);
  public static final Font DEFAULT_INPUT_FONT = new Font("Tahoma", Font.PLAIN, 11);

  private static final MaskFormatter getVersionMask() {
    try {
      return new MaskFormatter(GUIConstants.VERSION_MASK);
    } catch (ParseException e) {
      log.error(e.getMessage(), e);
    }
    return null;
  }

}
