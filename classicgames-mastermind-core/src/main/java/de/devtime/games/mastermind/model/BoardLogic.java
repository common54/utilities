package de.devtime.games.mastermind.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BoardLogic {

  private static final int DEFAULT_START_ROWS = 10;

  private List<CodeColor[]> codes;
  private List<MarkerColor[]> markers;
  private CodeColor[] codeToFind;
  private CodeColor activeColor;
  private int currentRow;
  private int codeLength;
  private int maxTries;
  private int originMaxTries;

  public BoardLogic(int codeLength) {
    this(codeLength, DEFAULT_START_ROWS);
  }

  public BoardLogic(int codeLength, int maxTries) {
    super();
    log.info("codeLength: {}, maxTries: {}", codeLength, maxTries);

    this.codeLength = codeLength;
    this.originMaxTries = maxTries;
    this.maxTries = maxTries;
    this.codes = new ArrayList<>();
    this.markers = new ArrayList<>();
    this.codeToFind = new CodeColor[codeLength];

    resetBoard();
  }

  public void setCode(int row, int column, CodeColor color) {
    this.codes.get(row)[column] = color;
  }

  public List<CodeColor[]> getCodes() {
    return this.codes;
  }

  public List<MarkerColor[]> getMarkers() {
    return this.markers;
  }

  public int getMaxTries() {
    return this.maxTries;
  }

  public int getCodeLength() {
    return this.codeLength;
  }

  public void generateCode() {
    log.info("generateCode");
    for (int i = 0; i < this.codeLength; i++) {
      this.codeToFind[i] = CodeColor.getRandomColor();
    }
    log.info("codeToFind: {}", Arrays.asList(this.codeToFind));
  }

  public void resetBoard() {
    log.info("resetBoard");
    for (int col = 0; col < this.codeLength; col++) {
      this.codeToFind[col] = CodeColor.EMPTY;
    }
    this.codes.clear();
    this.markers.clear();
    this.maxTries = this.originMaxTries;
    for (int row = 0; row < this.maxTries; row++) {
      this.codes.add(new CodeColor[this.codeLength]);
      this.markers.add(new MarkerColor[this.codeLength]);
      for (int col = 0; col < this.codeLength; col++) {
        this.codes.get(row)[col] = CodeColor.EMPTY;
        this.markers.get(row)[col] = MarkerColor.EMPTY;
      }
    }
    this.activeColor = CodeColor.EMPTY;
  }

  private void checkCode() {
    log.info("checkCode");

    if (isCurrentRowComplete()) {
      //      MarkerColor[] marker = new MarkerColor[this.codeLength];
      MarkerColor[] marker = this.markers.get(this.currentRow);
      correctColorPosition(marker);
      containsColor(marker);
      int correctColorAmount = 0;
      Arrays.sort(marker);
      for (int i = 0; i < this.codeLength; i++) {
        if (marker[i] == MarkerColor.BLACK) {
          correctColorAmount++;
          //          this.lblMarker[this.currentRow][i].setIcon(BLACK_SMALL);
        }
        if (marker[i] == MarkerColor.WHITE) {
          //          this.lblMarker[this.currentRow][i].setIcon(WHITE_SMALL);
        }
      }
      for (int col = 0; col < this.codeLength; col++) {
        //        this.currentCode[i] = CodeColor.EMPTY;
        this.codes.get(this.currentRow)[col] = CodeColor.EMPTY;
      }
      this.currentRow++;
      if (correctColorAmount == this.codeLength || this.currentRow == this.maxTries) {
        //        showCodeToFind();
      }
    }
  }

  private void correctColorPosition(MarkerColor[] marker) {
    log.info("marker: {}", Arrays.asList(marker));

    for (int col = 0; col < this.codeLength; col++) {
      if (this.codeToFind[col] == this.codes.get(this.currentRow)[col]) {
        marker[col] = MarkerColor.BLACK;
      }
    }
  }

  private void containsColor(MarkerColor[] marker) {
    log.info("marker: {}", Arrays.asList(marker));

    boolean[] used = new boolean[this.codeLength];
    for (int i = 0; i < this.codeLength; i++) {
      used[i] = marker[i] == MarkerColor.BLACK;
    }
    for (int currentCodeCol = 0; currentCodeCol < this.codeLength; currentCodeCol++) { // iterate over currentCode
      if (marker[currentCodeCol] != MarkerColor.BLACK) { // only check colors that does not already match with the code to find
        for (int codeToFindCol = 0; codeToFindCol < this.codeLength; codeToFindCol++) { // iterate over codeToFind
          if (!used[codeToFindCol] && marker[currentCodeCol] != MarkerColor.BLACK
              && this.codeToFind[codeToFindCol] == this.codes.get(this.currentRow)[currentCodeCol]) {
            marker[currentCodeCol] = MarkerColor.WHITE;
            used[codeToFindCol] = true;
            break;
          }
        }
      }
    }
  }

  private boolean isCurrentRowComplete() {
    log.info("isCurrentRowComplete");

    boolean isComplete = true;
    for (int col = 0; col < this.codeLength; col++) {
      if (this.codes.get(this.currentRow)[col] == CodeColor.EMPTY) {
        isComplete = false;
        break;
      }
    }
    return isComplete;
  }
}
