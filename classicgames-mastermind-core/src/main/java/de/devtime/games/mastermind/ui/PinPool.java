package de.devtime.games.mastermind.ui;

import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.lang3.RandomUtils;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.devtime.games.classicgames.core.GameController;
import de.devtime.games.mastermind.model.CodeColor;
import de.devtime.games.mastermind.screen.MastermindSinglePlayerGameScreen;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PinPool extends Group {

  private static final int PIN_AMOUNT = 5;

  private final GameController game;
  private final MastermindSinglePlayerGameScreen gameScreen;
  private final TextureAtlas atlas;
  private final CodeColor pinColor;

  public PinPool(GameController game, MastermindSinglePlayerGameScreen gameScreen, CodeColor pinColor, float tileSize) {
    super();
    log.info("create");

    this.game = game;
    this.gameScreen = gameScreen;
    this.atlas = game.getAtlas();
    this.pinColor = pinColor;

    setWidth(2 * tileSize);
    setHeight(1 * tileSize);

    creatUI(tileSize);
  }

  private void creatUI(float tileSize) {
    for (int i = 0; i < PIN_AMOUNT; i++) {
      Image pin = createPin(this.pinColor, tileSize);
      pin.addListener(new ClickListener() {

        @Override
        public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
          super.enter(event, x, y, pointer, fromActor);
          log.info("enter");
        }

        @Override
        public void clicked(InputEvent event, float x, float y) {
          super.clicked(event, x, y);
          log.info("clicked");
          PinPool.this.gameScreen.takePin(PinPool.this.pinColor);
          pin.setVisible(false);
          TimerTask task = new TimerTask() {

            @Override
            public void run() {
              pin.setVisible(true);
            }
          };
          Timer timer = new Timer("Pin visibility");
          timer.schedule(task, 5000);
        }

        @Override
        public boolean isPressed() {
          boolean pressed = super.isPressed();
          log.info("isPressed: {}", pressed);
          return pressed;
        }

        @Override
        public boolean isOver() {
          boolean over = super.isOver();
          log.info("isOver: {}", over);
          return over;
        }

      });
      addActor(pin);
    }
  }

  private Image createPin(CodeColor pinColor, float tileSize) {
    Image pin = new Image(this.atlas.findRegion(pinColor.getTextureName()));
    pin.setWidth(tileSize);
    pin.setHeight(tileSize);
    float x = RandomUtils.nextFloat(0f, getWidth() - tileSize);
    float y = RandomUtils.nextFloat(0f, getHeight() - tileSize);
    pin.setPosition(x, y);
    return pin;
  }
}
