package de.devtime.games.mastermind.screen;

import java.awt.Dimension;
import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import de.devtime.games.classicgames.core.GameController;
import de.devtime.games.classicgames.core.screen.AbstractScreen;
import de.devtime.games.mastermind.MastermindController;
import de.devtime.games.mastermind.model.CodeColor;
import de.devtime.games.mastermind.ui.MastermindBoard;
import de.devtime.games.mastermind.ui.PinPool;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MastermindSinglePlayerGameScreen extends AbstractScreen implements InputProcessor {

  private MastermindController controller;

  private MastermindBoard board;
  private Map<CodeColor, PinPool> pinPools = new HashMap<>();
  private OrthographicCamera camera;

  private Image selectedPin;
  private CodeColor selectedPinColor;
  private float tileSize;

  public MastermindSinglePlayerGameScreen(GameController game, MastermindController controller) {
    super(game);
    log.info("create");

    this.controller = controller;
  }

  @Override
  public void show() {
    //    super.show();
    log.info("show");

    this.selectedPin = new Image();

    Dimension viewDimension = this.game.getViewDimension();
    this.camera = new OrthographicCamera(viewDimension.width, viewDimension.height);
    //    this.camera.translate(0, 0);
    //    this.camera.translate(viewDimension.width / 2f, viewDimension.height / 2f);

    ScreenViewport screenViewport = new ScreenViewport(this.camera);
    this.batch = new SpriteBatch();
    this.stage = new Stage(screenViewport);

    log.info("camera position: {}", this.camera.position);

    //    Container<Table> tableContainer = new Container<>();
    float sw = this.game.getViewDimension().width;
    float sh = this.game.getViewDimension().height;
    //    float cw = sw * 0.5f;
    //    float ch = sh * 0.8f;
    //    tableContainer.setSize(cw, ch);
    //    tableContainer.setPosition((sw - cw) / 2.0f, (sh - ch) / 2.0f);
    //    tableContainer.setPosition(0, 0);
    //    tableContainer.fillX();
    //    tableContainer.fillY();
    //    tableContainer
    //        .setBackground(new TextureRegionDrawable(this.game.getAtlas().findRegion(AssetConstants.MENU_BACKGROUND)));

    int codeLength = 4;

    Table table = new Table();
    float padding = sh * 0.1f;
    this.tileSize = (sh - 2 * padding) / MastermindBoard.VISIBLE_ROWS;
    this.selectedPin.setWidth(this.tileSize);
    this.selectedPin.setHeight(this.tileSize);

    table.setPosition(2.5f * this.tileSize + padding, MastermindBoard.VISIBLE_ROWS / 2 * this.tileSize + padding);
    //    table.setDebug(true);

    //    this.back = new TextButton("Zurueck", skin);
    //    this.apply = new TextButton("Anwenden", skin);

    //    table.row().colspan(3).expand();

    this.board = new MastermindBoard(this.game, this, codeLength, this.tileSize);
    table.add(this.board);
    //    table.row().pad(0, 10, 10, 10);
    //    table.add(this.back).width(200).height(40).align(Align.left);
    //    table.add(new Label("", skin)).expandX();
    //    table.add(this.apply).width(200).height(40).align(Align.right);

    //    tableContainer.setActor(table);
    CodeColor[] colorsToUse = {
        CodeColor.RED, CodeColor.ORANGE, CodeColor.YELLOW, CodeColor.GREEN, CodeColor.CYAN, CodeColor.BLUE,
        CodeColor.VIOLET, CodeColor.PINK, CodeColor.BLACK
    };
    int row = 0;
    int column = 0;
    for (CodeColor color : colorsToUse) {
      log.info("color: {}", color.getTextureName());
      //      if (row == colorsToUse.length / 2) {
      //        column++;
      //        row = 0;
      //      }
      PinPool pinPool = new PinPool(this.game, this, color, this.tileSize);
      //      pinPool.setDebug(true);
      float xOffset = column * pinPool.getWidth();
      float yOffset = row * pinPool.getHeight();
      float x = viewDimension.width - padding - 2 * pinPool.getWidth() + xOffset;
      float y = viewDimension.height - padding - pinPool.getHeight() - yOffset;
      pinPool.setPosition(x, y);
      this.pinPools.put(color, pinPool);
      this.stage.addActor(pinPool);
      row++;
    }
    this.stage.addActor(table);
    this.stage.addActor(this.selectedPin);
    //    this.stage.setScrollFocus(tableContainer);

    this.camera.update();

    InputProcessor inputProcessorOne = this;
    InputProcessor inputProcessorTwo = this.stage;
    InputMultiplexer inputMultiplexer = new InputMultiplexer(inputProcessorOne, inputProcessorTwo);
    Gdx.input.setInputProcessor(inputMultiplexer);
    //    Gdx.input.setInputProcessor(new InputAdapter() {
    //      @Override
    //      public boolean scrolled(float amountX, float amountY) {
    //        log.info("amountX: {}, amountY: {}, zoom: {}", amountX, amountY,
    //            MastermindSinglePlayerGameScreen.this.camera.zoom);
    //        if (amountY == -1.0f) {
    //          log.info("zoom in");
    //          if (MastermindSinglePlayerGameScreen.this.camera.zoom > 1.0f) {
    //            MastermindSinglePlayerGameScreen.this.camera.zoom -= 0.05f;
    //          }
    //        } else {
    //          log.info("zoom out");
    //          if (MastermindSinglePlayerGameScreen.this.camera.zoom < 2.0f) {
    //            MastermindSinglePlayerGameScreen.this.camera.zoom += 0.05f;
    //          }
    //        }
    //        return true;
    //      }
    //    });
  }

  @Override
  public void hide() {
    log.info("hide");
    super.hide();
  }

  @Override
  public void pause() {
    log.info("pause");
  }

  @Override
  public void resume() {
    log.info("resume");
  }

  @Override
  public void resize(int width, int height) {
    log.info("width: {}, height: {}", width, height);

    this.stage.getViewport().update(width, height, true);
  }

  @Override
  public void render(float delta) {
    Gdx.gl.glClearColor(0f, 0f, 0f, 1);
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

    this.batch.begin();
    renderBackground(this.batch);
    this.batch.end();

    if (Gdx.input.isKeyJustPressed(Input.Keys.PAGE_UP)) {
      log.info("zoom in");
      this.camera.zoom -= delta;
    }
    if (Gdx.input.isKeyPressed(Input.Keys.PAGE_DOWN)) {
      log.info("zoom out");
      this.camera.zoom += delta;
    }

    this.stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
    this.stage.draw();
    this.camera.update();
  }

  @Override
  public void dispose() {
    log.info("dispose");

    super.dispose();
  }

  public CodeColor getSelectedPinColor() {
    return this.selectedPinColor;
  }

  public void takePin(CodeColor pin) {
    log.info("pin: {}", pin);

    this.selectedPinColor = pin;
    this.selectedPin.setDrawable(new TextureRegionDrawable(this.atlas.findRegion(pin.getTextureName())));
    Vector3 position = this.camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
    this.selectedPin.setPosition(position.x - this.tileSize / 2, position.y - this.tileSize / 2);
    this.selectedPin.setVisible(true);
  }

  public void dropPin() {
    log.info("dropPin");

    this.selectedPinColor = null;
    this.selectedPin.setVisible(false);
  }

  @Override
  public boolean keyDown(int keycode) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean keyUp(int keycode) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean keyTyped(char character) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean touchDown(int screenX, int screenY, int pointer, int button) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean touchUp(int screenX, int screenY, int pointer, int button) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean touchDragged(int screenX, int screenY, int pointer) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean mouseMoved(int screenX, int screenY) {
    log.info("screenX: {}, screenY: {}", screenX, screenY);
    if (this.selectedPinColor != null) {
      Vector3 position = this.camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
      this.selectedPin.setPosition(position.x - this.tileSize / 2, position.y - this.tileSize / 2);
    }
    return true;
  }

  @Override
  public boolean scrolled(float amountX, float amountY) {
    // TODO Auto-generated method stub
    return false;
  }
}
