package de.devtime.games.mastermind;

import java.awt.Color;
import java.awt.Component;
import java.awt.Insets;

import javax.swing.border.LineBorder;

public class InputFieldLineBorder extends LineBorder {

  private static final long serialVersionUID = 1L;

  public InputFieldLineBorder(Color color, int thickness) {
    super(color, thickness);
  }

  @Override
  public Insets getBorderInsets(Component c) {
    return new Insets(1, 2, 1, 2);
  }
}
