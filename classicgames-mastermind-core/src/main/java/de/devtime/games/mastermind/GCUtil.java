package de.devtime.games.mastermind;

import java.awt.GridBagConstraints;
import java.awt.Insets;

public class GCUtil {

  public static final int WEST = GridBagConstraints.WEST;
  public static final int SOUTH = GridBagConstraints.SOUTH;
  public static final int EAST = GridBagConstraints.EAST;
  public static final int NORTH = GridBagConstraints.NORTH;
  public static final int SOUTHWEST = GridBagConstraints.SOUTHWEST;
  public static final int SOUTHEAST = GridBagConstraints.SOUTHEAST;
  public static final int NORTHWEST = GridBagConstraints.NORTHWEST;
  public static final int NORTHEAST = GridBagConstraints.NORTHEAST;
  public static final int NONE = GridBagConstraints.NONE;
  public static final int HORI = GridBagConstraints.HORIZONTAL;
  public static final int VERTICAL = GridBagConstraints.VERTICAL;
  public static final int BOTH = GridBagConstraints.BOTH;

  public static void configureGC(GridBagConstraints gc, int gridx, int gridy, int anchor, int fill, double weightx,
      double weighty, int gridwidth, int gridheight, Insets insets) {
    gc.gridx = gridx;
    gc.gridy = gridy;
    gc.anchor = anchor;
    gc.fill = fill;
    gc.weightx = weightx;
    gc.weighty = weighty;
    gc.gridwidth = gridwidth;
    gc.gridheight = gridheight;
    gc.insets = insets;
  }
}
