package de.devtime.games.mastermind.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import de.devtime.games.classicgames.core.GameController;
import de.devtime.games.classicgames.core.screen.AbstractScreen;
import de.devtime.games.mastermind.MastermindController;
import de.devtime.games.mastermind.ui.MastermindMainMenu;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MastermindMainMenuScreen extends AbstractScreen implements MastermindController {

  private static final int PADDING = 50;

  private MastermindSettingsScreen settingsScreen;
  private MastermindSinglePlayerGameScreen singlePlayerScreen;

  private MastermindMainMenu mainMenu;

  public MastermindMainMenuScreen(GameController game) {
    super(game);
    log.info("create");

    this.settingsScreen = new MastermindSettingsScreen(game, this);
    this.singlePlayerScreen = new MastermindSinglePlayerGameScreen(game, this);
  }

  @Override
  public void show() {
    super.show();
    log.info("show");

    int screenWidth = this.game.getViewDimension().width;

    this.mainMenu = new MastermindMainMenu(this.game, this);

    Table table = new Table();
    table.setFillParent(true);
    table.top().left();
    table.row();
    table.add().width(PADDING);
    table.add(this.mainMenu);
    table.add().width(PADDING);

    this.stage.addActor(table);
  }

  @Override
  public void hide() {
    log.info("hide");
    super.hide();
  }

  @Override
  public void pause() {
    log.info("pause");
  }

  @Override
  public void resume() {
    log.info("resume");
  }

  @Override
  public void resize(int width, int height) {
    log.info("width: {}, height: {}", width, height);

    this.stage.getViewport().update(width, height, true);
  }

  @Override
  public void render(float delta) {
    Gdx.gl.glClearColor(0f, 0f, 0f, 1);
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

    this.batch.begin();
    renderBackground(this.batch);
    this.batch.end();

    this.stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
    this.stage.draw();
  }

  @Override
  public void changeScreen(View view) {
    switch (view) {
      case MAIN_MENU:
        this.game.setScreen(this);
      break;
      case SETTINGS:
        this.game.setScreen(this.settingsScreen);
      break;
      case SINGLEPLAYER:
        this.game.setScreen(this.singlePlayerScreen);
      break;
      default:
        throw new IllegalArgumentException("Unexpected value: " + view);
    }
  }
}
