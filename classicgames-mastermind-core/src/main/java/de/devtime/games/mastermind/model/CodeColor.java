package de.devtime.games.mastermind.model;

import org.apache.commons.lang3.RandomUtils;

public enum CodeColor {

  EMPTY("code-empty"),
  RED("code-red"),
  ORANGE("code-orange"),
  YELLOW("code-yellow"),
  GREEN("code-green"),
  CYAN("code-cyan"),
  BLUE("code-blue"),
  VIOLET("code-violet"),
  PINK("code-pink"),
  BROWN("code-brown"),
  GREY("code-grey"),
  WHITE("code-white"),
  BLACK("code-black");

  public static CodeColor getRandomColor() {
    CodeColor[] values = values();
    return values[RandomUtils.nextInt(1, values.length)];
  }

  private final String textureName;

  CodeColor(String textureName) {
    this.textureName = textureName;
  }

  public String getTextureName() {
    return this.textureName;
  }

}
