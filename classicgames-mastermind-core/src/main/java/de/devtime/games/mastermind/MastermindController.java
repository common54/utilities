package de.devtime.games.mastermind;

public interface MastermindController {

  public enum View {
    MAIN_MENU, SETTINGS, SINGLEPLAYER
  }

  void changeScreen(View view);

}
