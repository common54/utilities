package de.devtime.games.mastermind.ui;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable;

import de.devtime.games.classicgames.core.AssetConstants;
import de.devtime.games.classicgames.core.GameController;
import de.devtime.games.mastermind.model.BoardLogic;
import de.devtime.games.mastermind.model.CodeColor;
import de.devtime.games.mastermind.model.MarkerColor;
import de.devtime.games.mastermind.screen.MastermindSinglePlayerGameScreen;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MastermindBoard extends Table {

  public static final int VISIBLE_ROWS = 12;

  private final GameController game;
  private final MastermindSinglePlayerGameScreen gameScreen;
  private final TextureAtlas atlas;
  private final BoardLogic logic;

  private TextureRegion[] mastercodeTextures;
  private List<TextureRegion[]> markerTextures = new ArrayList<>();
  private List<TextureRegion[]> codeTextures = new ArrayList<>();

  private float codeTileSize;
  private float markerTileSize;

  public MastermindBoard(GameController game, MastermindSinglePlayerGameScreen gameScreen, int columns,
      float tileSize) {
    super();
    log.info("game: {}, columns: {}");

    this.game = game;
    this.gameScreen = gameScreen;
    this.atlas = game.getAtlas();
    this.logic = new BoardLogic(columns);

    this.codeTileSize = tileSize;
    this.markerTileSize = tileSize * 0.4f;

    initializeTextures(columns);
    createUI(columns);
  }

  @Override
  public void draw(Batch batch, float parentAlpha) {
    List<MarkerColor[]> markers = this.logic.getMarkers();
    for (int row = 0; row < this.logic.getMaxTries(); row++) {
      for (int col = 0; col < this.logic.getCodeLength(); col++) {
        this.markerTextures.get(row)[col] = this.atlas.findRegion(markers.get(row)[col].getTextureName());
      }
    }

    super.draw(batch, parentAlpha);
  }

  private void initializeTextures(int columns) {
    this.mastercodeTextures = new TextureRegion[columns];
    for (int col = 0; col < columns; col++) {
      this.mastercodeTextures[col] = this.atlas.findRegion(AssetConstants.BOARD_EMPTY);
    }
    for (int row = 0; row < this.logic.getMaxTries(); row++) {
      TextureRegion[] marker = new TextureRegion[columns];
      TextureRegion[] codes = new TextureRegion[columns];
      for (int col = 0; col < columns; col++) {
        marker[col] = this.atlas.findRegion(AssetConstants.MARKER_EMPTY);
      }
      for (int col = 0; col < columns; col++) {
        codes[col] = this.atlas.findRegion(AssetConstants.BOARD_EMPTY);
      }
      this.markerTextures.add(marker);
      this.codeTextures.add(codes);
    }
  }

  private void createUI(int columns) {
    log.info("createUI");

    TiledDrawable background = new TiledDrawable(this.atlas.findRegion(AssetConstants.BOARD_BACKGOUND));

    setBackground(background);
    add(createMastercode(columns)).right();
    row();
    for (int row = this.logic.getMaxTries() - 1; row >= 0; row--) {
      Table rowTable = new Table();
      Table markerTable = createMarkerTable(row, columns);
      Table codeTable = createCodeTable(row, columns);
      rowTable.add(markerTable);
      rowTable.add(codeTable);

      add(rowTable);
      row();
    }
  }

  private Table createMastercode(int columns) {
    Skin skin = this.game.getAssets().get(AssetConstants.SGX_SKIN_JSON, Skin.class);

    Table codeTable = new Table();
    codeTable.add(new Label("", skin));
    for (int col = 0; col < columns; col++) {
      Image image = new Image(this.mastercodeTextures[col]);
      codeTable.add(image).width(this.codeTileSize).height(this.codeTileSize);
    }
    codeTable.row();
    codeTable.add(new Label("", skin));
    for (int col = 0; col < columns; col++) {
      codeTable.add(new Label("", skin)).width(this.codeTileSize).height(this.codeTileSize);
    }
    return codeTable;
  }

  private Table createMarkerTable(int row, int columns) {
    Table markerTable = new Table();
    int offset = columns % 2 == 0 ? 0 : 1;
    int half = (columns + offset) / 2;
    for (int col = 0; col < columns; col++) {
      final int column = col;
      if (col == half) {
        markerTable.row();
      }
      Image image = new Image(this.markerTextures.get(row)[col]);
      image.addListener(new ClickListener() {

        @Override
        public void clicked(InputEvent event, float x, float y) {
          log.info("marker - event: {}, x: {}, y: {}, row: {}, col: {}", event, x, y, row, column);
        }

      });
      markerTable.add(image).width(this.markerTileSize).height(this.markerTileSize);
    }
    return markerTable;
  }

  private Table createCodeTable(int row, int columns) {
    Table codeTable = new Table();
    for (int col = 0; col < columns; col++) {
      final int column = col;
      Image image = new Image(this.codeTextures.get(row)[col]);
      image.addListener(new ClickListener() {

        @Override
        public void clicked(InputEvent event, float x, float y) {
          log.info("code - event: {}, x: {}, y: {}, row: {}, col: {}", event, x, y, row, column);
          CodeColor selectedPinColor = MastermindBoard.this.gameScreen.getSelectedPinColor();
          log.info("selectedPinColor: {}", selectedPinColor);
          if (selectedPinColor != null) {
            MastermindBoard.this.logic.setCode(row, column, selectedPinColor);
            MastermindBoard.this.gameScreen.dropPin();
          }
        }

      });
      codeTable.add(image).width(this.codeTileSize).height(this.codeTileSize);
    }
    return codeTable;
  }
}
