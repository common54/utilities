package de.devtime.games.mastermind.model;

public enum MarkerColor {

  EMPTY("marker-empty"),
  WHITE("marker-white"),
  BLACK("marker-black");

  private final String textureName;

  MarkerColor(String textureName) {
    this.textureName = textureName;
  }

  public String getTextureName() {
    return this.textureName;
  }

}
