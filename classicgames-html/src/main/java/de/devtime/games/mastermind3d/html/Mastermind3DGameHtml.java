package de.devtime.games.mastermind3d.html;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;

import de.devtime.games.mastermind3d.core.Mastermind3DGame;

public class Mastermind3DGameHtml extends GwtApplication {

  @Override
  public GwtApplicationConfiguration getConfig() {
    return new GwtApplicationConfiguration(480, 320);
  }

  @Override
  public ApplicationListener createApplicationListener() {
    return new Mastermind3DGame();
  }

}
