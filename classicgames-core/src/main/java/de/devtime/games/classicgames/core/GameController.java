package de.devtime.games.classicgames.core;

import java.awt.Dimension;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import de.devtime.games.classicgames.core.model.GameItem.ClassicGame;
import de.devtime.games.classicgames.core.screen.View;

public interface GameController {

  void restartWithNewGraphicSettings();

  void startGame(ClassicGame game);

  AssetManager getAssets();

  TextureAtlas getAtlas();

  boolean isMaximized();

  Dimension getViewDimension();

  void changeScreen(View screen);

  GameSettings getSettings();

  void exit();

  void setScreen(Screen screen);

}
