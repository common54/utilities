package de.devtime.games.classicgames.core.helper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class ObservableImpl<T> implements IObservable<T> {

  private final List<IObserver<T>> observers = new ArrayList<>();

  private boolean isChanging;

  @Override
  public void addObserver(final IObserver<T> obs) {
    this.observers.add(obs);
  }

  public final void clearChanged() {
    this.isChanging = false;
  }

  @Override
  public final void deleteObservers() {
    this.observers.clear();

  }

  public final boolean isChanging() {
    return this.isChanging;
  }

  @Override
  public final void notifyObservers(final IObservable<T> obs) {
    notifyObservers(obs, null);
  }

  @Override
  public final void notifyObservers(final IObservable<T> obs, final T updateObject) {
    if (this.isChanging) {
      final Iterator<IObserver<T>> iter = this.observers.iterator();
      while (iter.hasNext()) {
        final IObserver<T> observer = iter.next();
        observer.update(obs, updateObject);
      }
    }
  }

  @Override
  public final void removeObserver(final IObserver<T> obs) {
    this.observers.remove(obs);

  }

  public final void setChanged() {
    this.isChanging = true;
  }

  private final void sync(T updateFlag) {
    setChanged();
    notifyObservers(this, updateFlag);
    clearChanged();
  }

  protected final void syncViews() {
    syncViews(null);
  }

  protected final void syncViews(final T updateFlag) {
    sync(updateFlag);
  }
}
