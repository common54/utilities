package de.devtime.games.classicgames.core.screen;

import java.awt.Dimension;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import de.devtime.games.classicgames.core.AssetConstants;
import de.devtime.games.classicgames.core.GameController;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractScreen extends ScreenAdapter {

  protected final GameController game;
  protected final AssetManager assets;
  protected TextureAtlas atlas;
  protected Stage stage;
  protected SpriteBatch batch;

  private TiledDrawable background;

  protected AbstractScreen(GameController game) {
    super();

    this.game = game;
    this.assets = game.getAssets();

    this.atlas = this.assets.get(AssetConstants.IMAGES_ATLAS, TextureAtlas.class);
    this.background = new TiledDrawable(this.atlas.findRegion(AssetConstants.BACKGROUND));
  }

  @Override
  public void show() {
    log.info("show");

    this.batch = new SpriteBatch();
    this.stage = new Stage(new ScreenViewport());

    Gdx.input.setInputProcessor(this.stage);
  }

  @Override
  public void render(float delta) {
    this.stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
    this.stage.draw();
  }

  public void renderBackground(SpriteBatch batch) {
    Dimension viewDimension = getViewDimension();
    this.background.draw(batch, 0, 0, viewDimension.width, viewDimension.height);
  }

  @Override
  public void hide() {
    log.info("hide");
    this.dispose();
  }

  @Override
  public void dispose() {
    log.info("dispose");

    this.batch.dispose();
    this.stage.dispose();
  }

  protected Dimension getViewDimension() {
    Dimension dimension = new Dimension();
    dimension.width = Gdx.graphics.getDisplayMode().width;
    dimension.height = Gdx.graphics.getDisplayMode().height;
    if (!this.game.isMaximized()) {
      dimension.width = Gdx.graphics.getWidth();
      dimension.height = Gdx.graphics.getHeight();
    }
    return dimension;
  }
}
