package de.devtime.games.classicgames.core;

public class AssetConstants {

  public static final String IMAGES_ATLAS = "atlas/image-atlas.atlas";
  public static final String SGX_SKIN_JSON = "skin/sgx-ui.json";
  public static final String GLASSY_SKIN_JSON = "skin2/glassy-ui.json";

  public static String BACKGROUND = "background";
  public static String MENU_BACKGROUND = "menu-background";
  public static String BLACK_DOT = "black-dot";
  public static String BUTTON = "button";
  public static String BUTTON_HOVER = "button-hover";
  public static String BUTTON_START = "start";
  public static String BUTTON_START_HOVER = "start-hover";
  public static String BUTTON_SETTINGS = "settings";
  public static String BUTTON_SETTINGS_HOVER = "stettings-hover";
  public static String BUTTON_BACK = "back";
  public static String BUTTON_BACK_HOVER = "back-hover";
  public static String BUTTON_EXIT = "exit";
  public static String BUTTON_EXIT_HOVER = "exit-hover";

  public static String BOARD_BACKGOUND = "board-background";
  public static String MARKER_EMPTY = "marker-empty";
  public static String MARKER_BLACK = "marker-black";
  public static String MARKER_WHITE = "marker-white";
  public static String BOARD_EMPTY = "code-empty";

  private AssetConstants() {
    // private constant class constructor
  }
}
