package de.devtime.games.classicgames.core.sprite;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;

import de.devtime.games.classicgames.core.AssetConstants;
import de.devtime.games.classicgames.core.GameController;
import de.devtime.games.classicgames.core.helper.IObservable;
import de.devtime.games.classicgames.core.helper.IObserver;
import de.devtime.games.classicgames.core.model.DisplayModeItem;
import de.devtime.games.classicgames.core.model.FullscreenModeItem;
import de.devtime.games.classicgames.core.model.MonitorItem;
import de.devtime.games.classicgames.core.model.SettingsScreenModel;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GraphicSettings extends Table implements IObserver<Long> {

  private final GameController game;
  private final SettingsScreenModel model;

  private Label lblGraphicSettings;
  private Label lblFullscreenMode;
  private Label lblMonitors;
  private Label lblDisplayModes;
  private Label lblVSync;
  private SelectBox<FullscreenModeItem> sbFullscreenMode;
  private SelectBox<MonitorItem> sbMonitors;
  private SelectBox<DisplayModeItem> sbDisplayModes;
  private CheckBox cbVSync;
  private boolean listenerEnabled;

  public GraphicSettings(GameController game, SettingsScreenModel model) {
    super();
    log.info("create");

    this.game = game;
    this.model = model;
    this.model.addObserver(this);

    createUI();
  }

  private void createUI() {
    AssetManager assets = this.game.getAssets();
    Skin skin = assets.get(AssetConstants.SGX_SKIN_JSON, Skin.class);

    this.lblGraphicSettings = new Label("Grafik", skin);
    this.lblFullscreenMode = new Label("Vollbildmodus", skin);
    this.sbFullscreenMode = new SelectBox<>(skin);
    this.lblMonitors = new Label("Monitor", skin);
    this.sbMonitors = new SelectBox<>(skin);
    this.lblDisplayModes = new Label("Aufloesung", skin);
    this.sbDisplayModes = new SelectBox<>(skin);
    this.lblVSync = new Label("VSync", skin);
    this.cbVSync = new CheckBox("aktivieren", skin);

    row().colspan(2);
    add(this.lblGraphicSettings);
    row().pad(20, 0, 0, 0);
    add(this.lblFullscreenMode).pad(10).align(Align.bottomLeft);
    add(this.sbFullscreenMode).width(300).height(40).align(Align.left);
    row().pad(20, 0, 0, 0);
    add(this.lblMonitors).pad(10).align(Align.bottomLeft);
    add(this.sbMonitors).width(300).height(40).align(Align.left);
    row().pad(20, 0, 0, 0);
    add(this.lblDisplayModes).pad(10).align(Align.bottomLeft);
    add(this.sbDisplayModes).width(300).height(40).align(Align.left);
    row().pad(20, 0, 0, 0);
    add(this.lblVSync).pad(10).align(Align.bottomLeft);
    add(this.cbVSync).width(300).height(40).align(Align.left);
  }

  public void configureListener() {
    this.sbMonitors.addListener(new ChangeListener() {

      @Override
      public void changed(ChangeEvent event, Actor actor) {
        if (!GraphicSettings.this.model.isChanging()) {
          log.info("monitor changed: {}", GraphicSettings.this.sbMonitors.getSelected());
          GraphicSettings.this.model.setSelectedMonitor(GraphicSettings.this.sbMonitors.getSelected());
          event.handle();
        }
      }
    });
    this.sbDisplayModes.addListener(new ChangeListener() {

      @Override
      public void changed(ChangeEvent event, Actor actor) {
        if (!GraphicSettings.this.model.isChanging()) {
          log.info("display mode changed: {}", GraphicSettings.this.sbDisplayModes.getSelected());
          GraphicSettings.this.model.setSelectedDisplayMode(GraphicSettings.this.sbDisplayModes.getSelected());
          event.handle();
        }
      }
    });
    this.sbFullscreenMode.addListener(new ChangeListener() {

      @Override
      public void changed(ChangeEvent event, Actor actor) {
        if (!GraphicSettings.this.model.isChanging()) {
          log.info("fullscreen mode changed: {}", GraphicSettings.this.sbFullscreenMode.getSelected());
          GraphicSettings.this.model.setSelectedFullscreenMode(GraphicSettings.this.sbFullscreenMode.getSelected());
          event.handle();
        }
      }
    });
    this.cbVSync.addListener(new ChangeListener() {

      @Override
      public void changed(ChangeEvent event, Actor actor) {
        if (!GraphicSettings.this.model.isChanging()) {
          log.info("vsync changed: {}", GraphicSettings.this.cbVSync.isChecked());
          GraphicSettings.this.model.setVsync(GraphicSettings.this.cbVSync.isChecked());
          event.handle();
        }
      }
    });
  }

  @Override
  public void update(IObservable<Long> obs, Long updateEvent) {
    log.info("obs: {}, updateEvent: {}", obs, updateEvent);

    this.listenerEnabled = false;
    if (obs instanceof SettingsScreenModel) {
      if ((updateEvent & SettingsScreenModel.AVAILABLE_MONITORS_CHANGED) != 0) {
        this.sbMonitors.setItems(this.model.getAvailableMonitors());
      }
      if ((updateEvent & SettingsScreenModel.SELECTED_MONITOR_CHANGED) != 0) {
        this.sbMonitors.setSelected(this.model.getSelectedMonitor());
        log.info("this.model.getSelectedMonitor(): {}", this.model.getSelectedMonitor());
      }
      if ((updateEvent & SettingsScreenModel.AVAILABLE_DISPLAY_MODES_CHANGED) != 0) {
        this.sbDisplayModes.setItems(this.model.getAvailableDisplayModes());
      }
      if ((updateEvent & SettingsScreenModel.SELECTED_DISPLAY_MODE_CHANGED) != 0) {
        this.sbDisplayModes.setSelected(this.model.getSelectedDisplayMode());
        log.info("this.model.getSelectedDisplayMode(): {}", this.model.getSelectedDisplayMode());
      }
      if ((updateEvent & SettingsScreenModel.AVAILABLE_FULLSCREEN_MODES_CHANGED) != 0) {
        this.sbFullscreenMode.setItems(this.model.getAvailableFullscreenModes());
      }
      if ((updateEvent & SettingsScreenModel.SELECTED_FULLSCREEN_MODE_CHANGED) != 0) {
        this.sbFullscreenMode.setSelected(this.model.getSelectedFullscreenMode());
        log.info("this.model.getSelectedFullscreenMode(): {}", this.model.getSelectedFullscreenMode());
      }
      if ((updateEvent & SettingsScreenModel.VSYNC_CHANGED) != 0) {
        this.cbVSync.setChecked(this.model.isVsync());
      }
    }
    this.listenerEnabled = true;
  }
}
