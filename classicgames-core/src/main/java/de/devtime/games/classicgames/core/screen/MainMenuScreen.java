package de.devtime.games.classicgames.core.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import de.devtime.games.classicgames.core.GameController;
import de.devtime.games.classicgames.core.sprite.GamesCollection;
import de.devtime.games.classicgames.core.sprite.MainMenu;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MainMenuScreen extends AbstractScreen {

  private static final int PADDING = 50;

  private MainMenu mainMenu;
  private GamesCollection gamesCollection;

  public MainMenuScreen(GameController game) {
    super(game);

    log.info("create");
  }

  @Override
  public void show() {
    super.show();
    log.info("show");

    int screenWidth = this.game.getViewDimension().width;

    this.mainMenu = new MainMenu(this.game);
    this.gamesCollection = new GamesCollection(this.game, screenWidth - 3 * PADDING - MainMenu.MENU_WIDTH);

    Table table = new Table();
    table.setFillParent(true);
    table.top().left();
    table.row();
    table.add().width(PADDING);
    table.add(this.mainMenu);
    table.add().width(PADDING);
    table.add(this.gamesCollection).expand();
    table.add().width(PADDING);

    this.stage.addActor(table);
  }

  @Override
  public void hide() {
    log.info("hide");
    super.hide();
  }

  @Override
  public void pause() {
    log.info("pause");
  }

  @Override
  public void resume() {
    log.info("resume");
  }

  @Override
  public void resize(int width, int height) {
    log.info("width: {}, height: {}", width, height);

    this.stage.getViewport().update(width, height, true);
  }

  @Override
  public void render(float delta) {
    Gdx.gl.glClearColor(0f, 0f, 0f, 1);
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

    if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
      log.info("escape");
    }

    this.batch.begin();
    renderBackground(this.batch);
    this.batch.end();

    this.stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
    this.stage.draw();
  }
}
