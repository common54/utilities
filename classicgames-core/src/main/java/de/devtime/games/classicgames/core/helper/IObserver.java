package de.devtime.games.classicgames.core.helper;

public interface IObserver<T> {

  void update(IObservable<T> obs, T updateEvent);
}
