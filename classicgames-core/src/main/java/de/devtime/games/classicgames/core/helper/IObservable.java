package de.devtime.games.classicgames.core.helper;

public interface IObservable<T> {

  void addObserver(IObserver<T> obs);

  void deleteObservers();

  void notifyObservers(IObservable<T> obs);

  void notifyObservers(IObservable<T> obs, T obj);

  void removeObserver(IObserver<T> obs);
}
