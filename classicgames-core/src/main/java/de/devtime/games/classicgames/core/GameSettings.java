package de.devtime.games.classicgames.core;

import java.util.Objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics.DisplayMode;
import com.badlogic.gdx.Graphics.Monitor;
import com.badlogic.gdx.Preferences;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GameSettings {

  public static final String PREF_MONITOR = "monitor";
  public static final String PREF_DISPLAY_MODE = "displaymode";
  public static final String PREF_FULLSCREEN = "fullscreen";
  public static final String PREF_MAXIMIZED = "maximized";
  public static final String PREF_MUSIC_VOLUME = "volume";
  public static final String PREF_MUSIC_ENABLED = "music.enabled";
  public static final String PREF_SOUND_ENABLED = "sound.enabled";
  public static final String PREF_SOUND_VOL = "sound.volume";
  public static final String PREFS_NAME = "classicgames";
  public static final String PREF_VSYNC = "vsync";

  public static String buildDisplayModeConfigValue(DisplayMode displayMode) {
    return new StringBuilder()
        .append(displayMode.width)
        .append("x")
        .append(displayMode.height)
        .append("-")
        .append(displayMode.refreshRate)
        .toString();
  }

  protected Preferences getPrefs() {
    return Gdx.app.getPreferences(PREFS_NAME);
  }

  public Monitor getMonitor() {
    String monitorName = getPrefs().getString(PREF_MONITOR);
    Monitor monitorToUse = Gdx.graphics.getPrimaryMonitor();
    Monitor[] monitors = Gdx.graphics.getMonitors();
    for (Monitor monitor : monitors) {
      if (Objects.equals(monitor.name, monitorName)) {
        monitorToUse = monitor;
        break;
      }
    }
    log.info("Monitor to use: {}", monitorToUse.name);
    return monitorToUse;
  }

  public void setMonitor(Monitor monitor) {
    getPrefs().putString(PREF_MONITOR, monitor.name);
    getPrefs().flush();
  }

  public DisplayMode getDisplayMode() {
    Monitor monitor = getMonitor();
    String displayModeValue = getPrefs().getString(PREF_DISPLAY_MODE);
    DisplayMode displayModeToUse = Gdx.graphics.getDisplayMode(monitor);
    DisplayMode[] displayModes = Gdx.graphics.getDisplayModes(monitor);
    for (DisplayMode displayMode : displayModes) {
      if (Objects.equals(buildDisplayModeConfigValue(displayMode), displayModeValue)) {
        displayModeToUse = displayMode;
        break;
      }
    }
    return displayModeToUse;
  }

  public void setDisplayMode(DisplayMode displayMode) {
    getPrefs().putString(PREF_DISPLAY_MODE, buildDisplayModeConfigValue(displayMode));
    getPrefs().flush();
  }

  public boolean isFullscreen() {
    return getPrefs().getBoolean(PREF_FULLSCREEN, false);
  }

  public void setFullscreen(boolean fullscreen) {
    getPrefs().putBoolean(PREF_FULLSCREEN, fullscreen);
    getPrefs().flush();
  }

  public boolean isMaximized() {
    return getPrefs().getBoolean(PREF_MAXIMIZED, false);
  }

  public void setMaximized(boolean maximized) {
    getPrefs().putBoolean(PREF_MAXIMIZED, maximized);
    getPrefs().flush();
  }

  public boolean isSoundEffectsEnabled() {
    return getPrefs().getBoolean(PREF_SOUND_ENABLED, true);
  }

  public void setSoundEffectsEnabled(boolean soundEffectsEnabled) {
    getPrefs().putBoolean(PREF_SOUND_ENABLED, soundEffectsEnabled);
    getPrefs().flush();
  }

  public boolean isMusicEnabled() {
    return getPrefs().getBoolean(PREF_MUSIC_ENABLED, true);
  }

  public void setMusicEnabled(boolean musicEnabled) {
    getPrefs().putBoolean(PREF_MUSIC_ENABLED, musicEnabled);
    getPrefs().flush();
  }

  public float getMusicVolume() {
    return getPrefs().getFloat(PREF_MUSIC_VOLUME, 0.5f);
  }

  public void setMusicVolume(float volume) {
    getPrefs().putFloat(PREF_MUSIC_VOLUME, volume);
    getPrefs().flush();
  }

  public float getSoundVolume() {
    return getPrefs().getFloat(PREF_SOUND_VOL, 0.5f);
  }

  public void setSoundVolume(float volume) {
    getPrefs().putFloat(PREF_SOUND_VOL, volume);
    getPrefs().flush();
  }

  public boolean isVSyncEnabled() {
    return getPrefs().getBoolean(PREF_VSYNC, true);
  }

  public void setVSyncEnabled(boolean vsync) {
    getPrefs().putBoolean(PREF_VSYNC, vsync);
    getPrefs().flush();
  }
}
