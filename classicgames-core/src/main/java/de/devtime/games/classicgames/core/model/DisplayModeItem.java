package de.devtime.games.classicgames.core.model;

import java.util.Arrays;

import com.badlogic.gdx.Graphics.DisplayMode;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class DisplayModeItem {

  public static DisplayModeItem[] convert(DisplayMode[] displayModes) {
    return Arrays.asList(displayModes).stream()
        .map(DisplayModeItem::new)
        .toArray(DisplayModeItem[]::new);
  }

  private DisplayMode displayMode;
  private String title;

  public DisplayModeItem(DisplayMode displayMode) {
    super();
    this.displayMode = displayMode;
    StringBuilder tmp = new StringBuilder();
    tmp.append(displayMode.width).append("x").append(displayMode.height)
        .append(" - ").append(displayMode.refreshRate).append(" Hz");
    this.title = tmp.toString();
  }

  public DisplayMode getDisplayMode() {
    return this.displayMode;
  }

  @Override
  public String toString() {
    return this.title;
  }
}
