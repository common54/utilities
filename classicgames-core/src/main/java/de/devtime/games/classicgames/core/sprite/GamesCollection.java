package de.devtime.games.classicgames.core.sprite;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

import de.devtime.games.classicgames.core.AssetConstants;
import de.devtime.games.classicgames.core.GameController;
import de.devtime.games.classicgames.core.model.GameItem;
import de.devtime.games.classicgames.core.model.GameItem.ClassicGame;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GamesCollection extends Table {

  private static final int GAME_BUTTON_WIDTH = 300;
  private static final int GAME_BUTTON_HEIGHT = 100;
  private static final int GAME_BUTTON_PADDING = 10;

  private final GameController game;
  private final int freeSpace;

  public GamesCollection(GameController game, int freeSpace) {
    super();
    log.info("create: {}", freeSpace);

    this.game = game;
    this.freeSpace = freeSpace;

    createUI();
    configureListener();
  }

  private List<GameItem> getGameItems() {
    List<GameItem> gameList = new ArrayList<>();
    gameList.add(new GameItem(ClassicGame.MASTERMIND, "MasterMind"));
    return gameList;
  }

  private void createUI() {
    AssetManager assets = this.game.getAssets();
    Skin skin = assets.get(AssetConstants.SGX_SKIN_JSON, Skin.class);

    int columnAmount = this.freeSpace / GAME_BUTTON_WIDTH;
    while (this.freeSpace - columnAmount * GAME_BUTTON_WIDTH - columnAmount * GAME_BUTTON_PADDING * 2 < 0) {
      columnAmount--;
    }

    List<GameItem> gameList = getGameItems();

    row().expandX().fillX();
    add(new Label("Suche", skin));
    row().expandX().fillX();
    add(new Label("Favoriten", skin));
    row().expand().fill();
    Table gamesTable = new Table(skin);
    for (int i = 0; i < gameList.size(); i++) {
      if (i % columnAmount == 0) {
        gamesTable.row().pad(GAME_BUTTON_PADDING, GAME_BUTTON_PADDING, GAME_BUTTON_PADDING, GAME_BUTTON_PADDING)
            .width(GAME_BUTTON_WIDTH).height(GAME_BUTTON_HEIGHT);
      }
      GameItem gameItem = gameList.get(i);
      TextButton btnGame = new TextButton(gameItem.getTitle(), skin);
      btnGame.setWidth(GAME_BUTTON_WIDTH);
      btnGame.setHeight(GAME_BUTTON_HEIGHT);
      btnGame.addListener(new ChangeListener() {

        @Override
        public void changed(ChangeEvent event, Actor actor) {
          log.info("event: {}, actor: {}, game: {}", event, actor, gameItem.getGame());
          GamesCollection.this.game.startGame(gameItem.getGame());
        }
      });
      gamesTable.add(btnGame);
    }

    ScrollPane scrollPane = new ScrollPane(gamesTable);
    scrollPane.setScrollbarsVisible(true);

    add(scrollPane);

  }

  private void configureListener() {}
}
