package de.devtime.games.classicgames.core.sprite;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import de.devtime.games.classicgames.core.AssetConstants;
import de.devtime.games.classicgames.core.GameController;
import de.devtime.games.classicgames.core.screen.View;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MainMenu extends Table {

  public static final int MENU_WIDTH = 300;

  private static final int BORDER_THICKNESS = 3;
  private static final int BUTTON_WIDTH = 200;
  private static final int BUTTON_HEIGHT = 40;
  private static final int BUTTON_PADDING = 20;

  private final GameController game;

  private TextButton settings;
  private TextButton exit;

  public MainMenu(GameController game) {
    super();
    log.info("create");

    this.game = game;

    createUI();
    configureListener();
  }

  private void createUI() {
    AssetManager assets = this.game.getAssets();
    Skin skin = assets.get(AssetConstants.SGX_SKIN_JSON, Skin.class);

    this.settings = new TextButton("Einstellungen", skin);
    this.exit = new TextButton("Spiel beenden", skin);

    Table table = new Table(skin);
    table.setPosition(BORDER_THICKNESS, 0);
    table.setWidth(MENU_WIDTH - 2f * BORDER_THICKNESS);
    table.setHeight(this.game.getViewDimension().height);
    table.top().left().pad(50, (MENU_WIDTH - BUTTON_WIDTH) / 2f, 0, 0);
    table.row().width(5);
    table.add(this.settings).width(BUTTON_WIDTH).height(BUTTON_HEIGHT).uniformX();
    table.row().pad(BUTTON_PADDING, 0, 0, 0);
    table.add(this.exit).width(BUTTON_WIDTH).height(BUTTON_HEIGHT).uniformX();
    table.setBackground(new TextureRegionDrawable(this.game.getAtlas().findRegion(AssetConstants.MENU_BACKGROUND)));

    setWidth(MENU_WIDTH);
    setHeight(this.game.getViewDimension().height);
    add(new Image(new TextureRegionDrawable(this.game.getAtlas().findRegion(AssetConstants.BLACK_DOT))))
        .width(BORDER_THICKNESS)
        .height(this.game.getViewDimension().height);
    add(table)
        .width(MENU_WIDTH - 2f * BORDER_THICKNESS)
        .height(this.game.getViewDimension().height);
    add(new Image(new TextureRegionDrawable(this.game.getAtlas().findRegion(AssetConstants.BLACK_DOT))))
        .width(BORDER_THICKNESS)
        .height(this.game.getViewDimension().height);
  }

  private void configureListener() {
    this.settings.addListener(new ChangeListener() {
      @Override
      public void changed(ChangeEvent event, Actor actor) {
        MainMenu.this.game.changeScreen(View.SETTINGS);
      }
    });

    this.exit.addListener(new ChangeListener() {
      @Override
      public void changed(ChangeEvent event, Actor actor) {
        MainMenu.this.game.exit();
      }
    });
  }
}
