package de.devtime.games.classicgames.core.model;

public class GameItem {

  public enum ClassicGame {
    MASTERMIND;
  }

  private final ClassicGame game;
  private final String title;

  public GameItem(ClassicGame game, String title) {
    super();

    this.game = game;
    this.title = title;
  }

  public ClassicGame getGame() {
    return this.game;
  }

  public String getTitle() {
    return this.title;
  }
}
