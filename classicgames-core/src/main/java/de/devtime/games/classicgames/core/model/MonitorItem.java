package de.devtime.games.classicgames.core.model;

import java.util.Arrays;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics.DisplayMode;
import com.badlogic.gdx.Graphics.Monitor;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class MonitorItem {

  public static MonitorItem[] convert(Monitor[] monitors) {
    return Arrays.asList(monitors).stream()
        .map(MonitorItem::new)
        .toArray(MonitorItem[]::new);
  }

  private Monitor monitor;
  private String title;

  public MonitorItem(Monitor monitor) {
    super();
    this.monitor = monitor;
    DisplayMode displayMode = Gdx.graphics.getDisplayMode(monitor);
    StringBuilder tmp = new StringBuilder();
    tmp.append(monitor.name)
        .append(" (").append(displayMode.width).append("x").append(displayMode.height).append(")")
        .append(" ").append(displayMode.refreshRate).append(" Hz");
    this.title = tmp.toString();
  }

  public Monitor getMonitor() {
    return this.monitor;
  }

  @Override
  public String toString() {
    return this.title;
  }
}
