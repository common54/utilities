package de.devtime.games.classicgames.core.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

import de.devtime.games.classicgames.core.AssetConstants;
import de.devtime.games.classicgames.core.GameController;
import de.devtime.games.classicgames.core.model.DisplayModeItem;
import de.devtime.games.classicgames.core.model.FullscreenModeItem;
import de.devtime.games.classicgames.core.model.MonitorItem;
import de.devtime.games.classicgames.core.model.SettingsScreenModel;
import de.devtime.games.classicgames.core.sprite.GraphicSettings;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SettingsScreen extends AbstractScreen {

  private SelectBox<FullscreenModeItem> sbFullscreenMode;
  private SelectBox<MonitorItem> sbMonitors;
  private SelectBox<DisplayModeItem> sbDisplayModes;
  private TextButton back;
  private TextButton apply;

  private GraphicSettings graphicSettings;
  private SettingsScreenModel model;

  public SettingsScreen(GameController game) {
    super(game);
    log.info("create");

    this.model = new SettingsScreenModel(game.getSettings());
  }

  @Override
  public void show() {
    super.show();
    log.info("show");

    this.graphicSettings = new GraphicSettings(this.game, this.model);

    Skin skin = this.assets.get(AssetConstants.SGX_SKIN_JSON, Skin.class);

    Container<Table> tableContainer = new Container<>();
    float sw = this.game.getViewDimension().width;
    float sh = this.game.getViewDimension().height;
    float cw = sw * 0.6f;
    float ch = sh * 1.0f;
    tableContainer.setSize(cw, ch);
    tableContainer.setPosition((sw - cw) / 2.0f, (sh - ch) / 2.0f);
    tableContainer.fillX();
    tableContainer.fillY();
    tableContainer
        .setBackground(new TextureRegionDrawable(this.game.getAtlas().findRegion(AssetConstants.MENU_BACKGROUND)));

    Table table = new Table();

    this.back = new TextButton("Zurueck", skin);
    this.apply = new TextButton("Anwenden", skin);

    table.row().colspan(3).expand();
    table.add(this.graphicSettings);
    table.row().pad(0, 10, 10, 10);
    table.add(this.back).width(200).height(40).align(Align.left);
    table.add(new Label("", skin)).expandX();
    table.add(this.apply).width(200).height(40).align(Align.right);

    tableContainer.setActor(table);
    this.stage.addActor(tableContainer);

    this.model.initialize();

    this.graphicSettings.configureListener();
    configureListener();
  }

  private void configureListener() {
    this.back.addListener(new ChangeListener() {
      @Override
      public void changed(ChangeEvent event, Actor actor) {
        SettingsScreen.this.game.changeScreen(View.MAIN_MENU);
        event.handle();
      }
    });
    this.apply.addListener(new ChangeListener() {
      @Override
      public void changed(ChangeEvent event, Actor actor) {
        SettingsScreen.this.model.applySettings();
        SettingsScreen.this.game.restartWithNewGraphicSettings();
        event.handle();
      }
    });
  }

  @Override
  public void hide() {
    log.info("hide");
    super.hide();
  }

  @Override
  public void pause() {
    log.info("pause");
  }

  @Override
  public void resume() {
    log.info("resume");
  }

  @Override
  public void resize(int width, int height) {
    log.info("width: {}, height: {}", width, height);

    this.stage.getViewport().update(width, height, true);
  }

  @Override
  public void render(float delta) {
    Gdx.gl.glClearColor(0f, 0f, 0f, 1);
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

    this.batch.begin();
    renderBackground(this.batch);
    this.batch.end();

    this.stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
    this.stage.draw();
  }

  @Override
  public void dispose() {
    log.info("dispose");

    super.dispose();
  }
}
