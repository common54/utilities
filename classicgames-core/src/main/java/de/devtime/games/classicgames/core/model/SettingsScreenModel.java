package de.devtime.games.classicgames.core.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics.DisplayMode;
import com.badlogic.gdx.Graphics.Monitor;

import de.devtime.games.classicgames.core.GameSettings;
import de.devtime.games.classicgames.core.helper.MiscUtil;
import de.devtime.games.classicgames.core.helper.ObservableImpl;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SettingsScreenModel extends ObservableImpl<Long> {

  private static int pos = 0;
  public static final long AVAILABLE_FULLSCREEN_MODES_CHANGED = MiscUtil.setLongBit(pos++);
  public static final long SELECTED_FULLSCREEN_MODE_CHANGED = MiscUtil.setLongBit(pos++);
  public static final long AVAILABLE_MONITORS_CHANGED = MiscUtil.setLongBit(pos++);
  public static final long SELECTED_MONITOR_CHANGED = MiscUtil.setLongBit(pos++);
  public static final long AVAILABLE_DISPLAY_MODES_CHANGED = MiscUtil.setLongBit(pos++);
  public static final long SELECTED_DISPLAY_MODE_CHANGED = MiscUtil.setLongBit(pos++);
  public static final long VSYNC_CHANGED = MiscUtil.setLongBit(pos++);

  private GameSettings settings;
  private FullscreenModeItem[] availableFullscreenModes;
  private FullscreenModeItem selectedFullscreenMode;
  private MonitorItem[] availableMonitors;
  private MonitorItem selectedMonitor;
  private DisplayModeItem[] availableDisplayModes;
  private DisplayModeItem selectedDisplayMode;
  private boolean vsync;

  public SettingsScreenModel(GameSettings settings) {
    super();

    this.settings = settings;
  }

  public void initialize() {
    log.info("initialize");

    Monitor settingsMonitor = this.settings.getMonitor();
    DisplayMode settingsDisplayMode = this.settings.getDisplayMode();
    boolean settingsFullscreen = this.settings.isFullscreen();
    boolean settingsMaximized = this.settings.isMaximized();
    this.vsync = this.settings.isVSyncEnabled();

    initializeFullscreenMode(settingsFullscreen, settingsMaximized);
    initializeMonitor(settingsMonitor);
    initializeDisplayMode(settingsDisplayMode);

    long syncFlag = AVAILABLE_FULLSCREEN_MODES_CHANGED
        | SELECTED_FULLSCREEN_MODE_CHANGED
        | AVAILABLE_MONITORS_CHANGED
        | SELECTED_MONITOR_CHANGED
        | AVAILABLE_DISPLAY_MODES_CHANGED
        | SELECTED_DISPLAY_MODE_CHANGED
        | VSYNC_CHANGED;
    if (!isChanging()) {
      syncViews(syncFlag);
    }
  }

  private void initializeFullscreenMode(boolean settingsFullscreen, boolean settingsMaximized) {
    this.availableFullscreenModes = new FullscreenModeItem[] {
        new FullscreenModeItem(true, true),
        new FullscreenModeItem(false, true),
        new FullscreenModeItem(false, false)
    };
    for (FullscreenModeItem fullscreenModeItem : this.availableFullscreenModes) {
      if (fullscreenModeItem.isFullscreen() == settingsFullscreen
          && fullscreenModeItem.isMaximized() == settingsMaximized) {
        this.selectedFullscreenMode = fullscreenModeItem;
      }
    }
  }

  private void initializeMonitor(Monitor settingsMonitor) {
    log.info("settingsMonitor: {}", settingsMonitor.name);

    this.selectedMonitor = null;
    this.availableMonitors = MonitorItem.convert(Gdx.graphics.getMonitors());
    for (MonitorItem monitorItem : this.availableMonitors) {
      if (Objects.equals(monitorItem.getMonitor().name, settingsMonitor.name)) {
        this.selectedMonitor = monitorItem;
      }
    }
    if (this.selectedMonitor == null) {
      log.warn("Monitor from settings (" + settingsMonitor.name + ") not found. Use primary monitor ...");
      Monitor primaryMonitor = Gdx.graphics.getPrimaryMonitor();
      for (MonitorItem monitorItem : this.availableMonitors) {
        if (Objects.equals(monitorItem.getMonitor().name, primaryMonitor.name)) {
          this.selectedMonitor = monitorItem;
        }
      }
    }
  }

  private void initializeDisplayMode(DisplayMode settingsDisplayMode) {
    log.info("settingsDisplayMode: {}", settingsDisplayMode);

    filterDisplayModes();

    Optional<DisplayModeItem> optSelectDisplayMode = selectDisplayMode(this.availableDisplayModes, settingsDisplayMode);
    if (optSelectDisplayMode.isPresent()) {
      this.selectedDisplayMode = optSelectDisplayMode.get();
    } else {
      log.warn("Could not select a displayMode");
    }
  }

  public FullscreenModeItem getSelectedFullscreenMode() {
    return this.selectedFullscreenMode;
  }

  public void setSelectedFullscreenMode(FullscreenModeItem selectedFullscreenMode) {
    if (!Objects.equals(this.selectedFullscreenMode, selectedFullscreenMode)) {
      this.selectedFullscreenMode = selectedFullscreenMode;

      filterDisplayModes();

      if (!isChanging()) {
        syncViews(SELECTED_FULLSCREEN_MODE_CHANGED | AVAILABLE_DISPLAY_MODES_CHANGED);
      }
    }
  }

  public FullscreenModeItem[] getAvailableFullscreenModes() {
    return this.availableFullscreenModes;
  }

  public MonitorItem[] getAvailableMonitors() {
    return this.availableMonitors;
  }

  public MonitorItem getSelectedMonitor() {
    return this.selectedMonitor;
  }

  public void setSelectedMonitor(MonitorItem selectedMonitor) {
    if (!Objects.equals(this.selectedMonitor, selectedMonitor)) {
      this.selectedMonitor = selectedMonitor;

      if (this.selectedFullscreenMode != null) {
        filterDisplayModes();
      }

      if (!isChanging()) {
        syncViews(SELECTED_MONITOR_CHANGED | AVAILABLE_DISPLAY_MODES_CHANGED);
      }
    }
  }

  public DisplayModeItem getSelectedDisplayMode() {
    return this.selectedDisplayMode;
  }

  public void setSelectedDisplayMode(DisplayModeItem selectedDisplayMode) {
    if (!Objects.equals(this.selectedDisplayMode, selectedDisplayMode)) {
      this.selectedDisplayMode = selectedDisplayMode;

      if (!isChanging()) {
        syncViews(SELECTED_DISPLAY_MODE_CHANGED);
      }
    }
  }

  public DisplayModeItem[] getAvailableDisplayModes() {
    return this.availableDisplayModes;
  }

  public void setAvailableDisplayModes(DisplayModeItem[] availableDisplayModes) {
    log.info("availableDisplayModes: {}", Arrays.asList(availableDisplayModes));

    DisplayMode settingsDisplayMode = this.settings.getDisplayMode();
    this.selectedDisplayMode = null;
    this.availableDisplayModes = availableDisplayModes;
    Optional<DisplayModeItem> optDisplayModeToSelect = selectDisplayMode(availableDisplayModes, settingsDisplayMode);
    if (optDisplayModeToSelect.isPresent()) {
      setSelectedDisplayMode(optDisplayModeToSelect.get());
    } else {
      log.warn("Could not select a displayMode");
    }

    if (!isChanging()) {
      syncViews(AVAILABLE_DISPLAY_MODES_CHANGED);
    }
  }

  private Optional<DisplayModeItem> selectDisplayMode(DisplayModeItem[] availableDisplayModes,
      DisplayMode settingsDisplayMode) {
    for (DisplayModeItem displayModeItem : availableDisplayModes) {
      if (Objects.equals(GameSettings.buildDisplayModeConfigValue(displayModeItem.getDisplayMode()),
          GameSettings.buildDisplayModeConfigValue(settingsDisplayMode))) {
        return Optional.of(displayModeItem);
      }
    }
    if (this.selectedDisplayMode == null) {
      log.warn("Display mode from settings (" + GameSettings.buildDisplayModeConfigValue(settingsDisplayMode)
          + ") not found. Use primary display mode ...");
      DisplayMode primaryDisplayMode = Gdx.graphics.getDisplayMode(this.selectedMonitor.getMonitor());
      for (DisplayModeItem displayModeItem : availableDisplayModes) {
        if (Objects.equals(GameSettings.buildDisplayModeConfigValue(displayModeItem.getDisplayMode()),
            GameSettings.buildDisplayModeConfigValue(primaryDisplayMode))) {
          return Optional.of(displayModeItem);
        }
      }
    }
    return Optional.empty();
  }

  public boolean isVsync() {
    return this.vsync;
  }

  public void setVsync(boolean vsync) {
    log.info("vsync: {}", vsync);

    this.vsync = vsync;

    if (!isChanging()) {
      syncViews(VSYNC_CHANGED);
    }
  }

  public void applySettings() {
    this.settings.setFullscreen(this.selectedFullscreenMode.isFullscreen());
    this.settings.setMaximized(this.selectedFullscreenMode.isMaximized());
    this.settings.setMonitor(this.selectedMonitor.getMonitor());
    this.settings.setDisplayMode(this.selectedDisplayMode.getDisplayMode());
    this.settings.setVSyncEnabled(this.vsync);
  }

  private void filterDisplayModes() {
    if (this.selectedFullscreenMode != null && !this.selectedFullscreenMode.isFullscreen()
        && this.selectedFullscreenMode.isMaximized()) {
      DisplayMode currentDisplayMode = Gdx.graphics.getDisplayMode(getSelectedMonitor().getMonitor());
      List<DisplayModeItem> filteredDisplayModes = new ArrayList<>();
      DisplayModeItem[] displayModes = DisplayModeItem
          .convert(Gdx.graphics.getDisplayModes(this.selectedMonitor.getMonitor()));
      for (DisplayModeItem displayModeItem : displayModes) {
        DisplayMode dm = displayModeItem.getDisplayMode();
        if (dm.width == currentDisplayMode.width && dm.height == currentDisplayMode.height) {
          filteredDisplayModes.add(displayModeItem);
        }
      }
      this.availableDisplayModes = filteredDisplayModes.toArray(new DisplayModeItem[filteredDisplayModes.size()]);
      if (this.availableDisplayModes.length > 0) {
        this.selectedDisplayMode = this.availableDisplayModes[0];
      }
    } else {
      List<DisplayModeItem> filteredDisplayModes = new ArrayList<>();
      DisplayModeItem[] displayModes = DisplayModeItem
          .convert(Gdx.graphics.getDisplayModes(getSelectedMonitor().getMonitor()));
      for (DisplayModeItem displayModeItem : displayModes) {
        DisplayMode dm = displayModeItem.getDisplayMode();
        if (dm.width >= 800 && dm.height >= 600) {
          filteredDisplayModes.add(displayModeItem);
        }
      }
      this.availableDisplayModes = filteredDisplayModes.toArray(new DisplayModeItem[filteredDisplayModes.size()]);
    }
  }
}
