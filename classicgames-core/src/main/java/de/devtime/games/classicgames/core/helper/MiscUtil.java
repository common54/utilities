package de.devtime.games.classicgames.core.helper;

public final class MiscUtil {

  public static long removeLongBit(final long value, final int position) {
    if ((position < 0) || (position > 63)) {
      throw new IllegalArgumentException("Illegal position! (codomain [0;63])");
    }
    return value & ~setLongBit(position);
  }

  public static long removeLongBitsByMask(final long value, final long mask) {
    return value & ~mask;
  }

  public static long setAllBits() {
    return -1L;
  }

  public static long setLongBit(final int position) {
    if ((position < 0) || (position > 63)) {
      throw new IllegalArgumentException("Illegal position! (codomain [0;63])");
    }
    return 1L << position;
  }

  private MiscUtil() {
    super();
  }
}
