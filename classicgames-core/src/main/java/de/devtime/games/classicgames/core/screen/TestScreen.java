package de.devtime.games.classicgames.core.screen;

import org.apache.commons.lang3.RandomUtils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;

import de.devtime.games.classicgames.core.GameController;

public class TestScreen extends ScreenAdapter {

  private static final String IMG_WOODEN_TEXTURE_PATH = "wooden-texture-002.jpg";

  private GameController game;
  private AssetManager assets;
  private Texture background;
  private SpriteBatch batch;
  private ShapeRenderer renderer;
  private Vector2 position;
  private float maxVelocity = 25.0f;

  public TestScreen(GameController game) {
    super();

    this.game = game;
    this.assets = new AssetManager();
    this.assets.load(IMG_WOODEN_TEXTURE_PATH, Texture.class);
    this.batch = new SpriteBatch();
    this.renderer = new ShapeRenderer();
    this.position = new Vector2(0.0f, RandomUtils.nextFloat(0.0f, Gdx.graphics.getHeight()));
  }

  @Override
  public void render(float delta) {
    if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
      Gdx.app.exit();
    }
    if (Gdx.input.isButtonJustPressed(Input.Buttons.LEFT)) {
      System.out.println(Gdx.input.getX() + " - " + Gdx.input.getY());
    }

    this.position.x += this.maxVelocity * delta;

    if (this.assets.update()) {
      this.background = this.assets.get(IMG_WOODEN_TEXTURE_PATH, Texture.class);
      //      this.elapsed += Gdx.graphics.getDeltaTime();
      //      Gdx.gl.glClearColor(0, 0, 0, 0);
      //      Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
      this.batch.begin();
      //      this.batch.draw(this.background, 100 + 100 * (float) Math.cos(this.elapsed),
      //          100 + 25 * (float) Math.sin(this.elapsed));
      this.batch.draw(this.background, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
      this.batch.end();
      this.renderer.begin(ShapeType.Filled);
      this.renderer.setColor(0.6f, 0.0f, 0.0f, 1.0f);
      this.renderer.rect(50f, 0f, 100, Gdx.graphics.getHeight());
      this.renderer.setColor(Color.BLACK);
      this.renderer.circle(this.position.x, this.position.y, 3.0f);
      this.renderer.end();
    }
  }

  @Override
  public void dispose() {
    this.assets.dispose();
    this.background.dispose();
    this.batch.dispose();
    this.renderer.dispose();
  }

  @Override
  public void hide() {
    this.dispose();
  }
}
