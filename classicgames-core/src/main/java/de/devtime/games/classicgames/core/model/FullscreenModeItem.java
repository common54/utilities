package de.devtime.games.classicgames.core.model;

public class FullscreenModeItem {

  private final boolean fullscreen;
  private final boolean maximized;

  public FullscreenModeItem(boolean fullscreen, boolean maximized) {
    super();

    this.fullscreen = fullscreen;
    this.maximized = maximized;
  }

  public boolean isMaximized() {
    return this.maximized;
  }

  public boolean isFullscreen() {
    return this.fullscreen;
  }

  @Override
  public String toString() {
    String result;
    if (this.fullscreen) {
      result = "Vollbild";
    } else if (this.maximized) {
      result = "Vollbild - Fenstermodus";
    } else {
      result = "Fenstermodus";
    }
    return result;
  }
}
